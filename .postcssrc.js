const path = require('path');

module.exports = ({ file }) => {
  // const designWidth = file.dirname.includes(path.join('node_modules', 'vant')) ? 375 : 750;
  // 项目设计稿是375，vant设计稿也是375,所以就不用换算了。
  // console.log(designWidth)
  return {
    plugins: {
      autoprefixer: {},
      "postcss-px-to-viewport": {
        unitToConvert: "px",
        viewportWidth: 375,
        unitPrecision: 6,
        propList: ["*"],
        viewportUnit: "vw",
        fontViewportUnit: "vw",
        selectorBlackList: [],
        minPixelValue: 1,
        mediaQuery: true,
        exclude: [],
        landscape: false
      }
    }
  }

}

