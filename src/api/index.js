import post from '@/utils/authPost';
import request from '@/utils/request'
// 
// let api = "/api"
let api = "/dpi/bc-prize-customer"  //*
let bpi = "/bpi"
let cpi = "/dpi/bc-prize-shopping-cart"
let dpi = "/dpi/bc-prize-marketing"	
let epi = "/dpi/bc-prize-pay" 
let fpi = "/epi"

let mpi = "/mpi/bc-authorize"
// let gpi = "/gpi/bc-guess-up-down" //dev  gpi

let gpi = "/dpi/bc-guess-up-down" //uat  dpi  



export const payTest = (data) => {
return request({
		url: fpi+'/bc-pay/v1/pay/json',
		method: 'post',
		data
	})
};


//大盘信息
export const Market = (data) =>{
	return request ({
		url: gpi+'/market/message',
		method:'get',
		data
	})
};

//添加大盘信息   ===> //后端调试
export const set_Market = (data) =>{
	return request ({
		url: gpi+'/guess/info/addGuessInfo',
		method:'post',
		data
	})
};


//奖金池信息
export const Prize_Pool = (data) =>{
	return request ({
		url: gpi+'/guess/info/getGuessPrizePool',
		method:'post',
		data
	})
};
// 商品详情页
export const goods_Details = (data) =>{
	return request ({
		url: gpi+'/award/product/info/list',
		method:'post',
		data
	})
};
//获取首页的三个奖品数据
export const getMarkets = () =>{
	return request ({
		url: gpi+'/award/product/info/in/index',
		method:'get'
	})
};
//排行榜
export const topCharts = (data) =>{
	return request ({
		url: gpi+'/user/historyRecord/topList',
		method:'post',
		data
	})
};

//用户个人战绩
export const personRecord = (data) =>{
	return request ({
		url: gpi+'/history/record/user',
		method:'post',
		data
	})
};
//历史战绩
export const HistoryResult = (data) =>{
	return request ({
		url: gpi+'/user/historyRecord/list',
		method:'post',
		data
	})
};

//用户贝币明细   
export const bbDetails = (data) =>{
	return request ({
		url: gpi+'/user/beibi/info',
		method:'post',
		data
	})
};
//用户-商品券码
export const coupon = (data) =>{
	return request ({
		url: gpi+'/personal/center/couponCode',
		method:'post',
		data
	})
};

//竞猜
export const GuessUpAnDown = (data) =>{
	return request ({
		url: gpi+'/user/beibi/guess',
		method:'post',
		data
	})
};

//用户是否参加过竞猜
export const GuessUpAnDownWhether = (data) =>{
	return request ({
		url: gpi+'/user/beibi/whether/guess',
		method:'post',
		data
	})
};
//签到领奖 
export const SignIn = (data) =>{
	return request ({
		url: gpi+'/sign/beibi/award',
		method:'post',
		data
	})
};

// 大盘每分钟信息
export const getChartInfo = data =>{
	return request ({
		url: gpi+'/market/message/detail',
		method:'get',
		data
	})
};

export const upDown = (data) =>{
	return request ({
		url: gpi+'/user/beibi/join',
		method:'post',
		data
	})
};

// 获得C端会员信息, 猜涨跌个人用户信息 可用于分享
export const memberInfo= (data)=>{
	return request({
		url: gpi+'/member/customer/info/get',
		method: 'post',
		data
	})
}

// 登录时带分享的参数
export const memberLogin = (data) => {
	return request({
		url: api+'/member/login',
		method: 'post',
		data
	}
	// {withCredentials: true}
	)
  };








// 记录事件点击分析
export const _bc = (data) => {
return request({
		url: api+'/interface/log',
		method: 'post',
		data
	})
};
// 首页访问
// export const interfaceHome = () => {
// 	return post({
// 	  url: api+'/interface/home'
// 	});
// };
// 获取页面信息

export const getCode = () => {
  return post({
    url: '/api/v1/p/page'
  });
};

// 登录
/*{
  "mobile":"18516520359",                //类型：String  必有字段  备注：手机号
  "verificationCode":"589470",                //类型：String  必有字段  备注：验证码
  "deviceId":"3ecd660c-5863-48e8-9ed0-4ed275deb444",                //类型：String  必有字段  备注：设备id
  "source":"1"                //类型：String  必有字段  备注：注册来源1银行兑换码页面注册 2 小程序注册 3 APP注册
} */

// 获取验证码
/*{
  "mobile":"18516520359",                //类型：String  必有字段  备注：手机号
  "uuid":"5446b45ebbd6482498a89514e81ade25"                //类型：String  必有字段  备注：唯一标识
}*/
export const verificationCode = (data) => {
  return request({
		url: api+'/verification/code',
		method: 'post',
		data
	})
};

/**
 * 首页广告展示
 * {
    "advertiseLocationNo":"mock",                //类型：String  必有字段  备注：广告位置编码
    "channelNo":"mock"                //类型：String  必有字段  备注：渠道编号
  }
 */
export const advertiseShow = data => {
	return request({
		url: api+'/banner/show',
		method: 'post',
		data
	})
};

// 商品分类
export const productTypeList = data => {
	return request({
		url: api+'/product/classify/list',
		method: 'get'
  })
}
/**
 * 商品展示 根据商品分类查询
 * {
    "channelNo":"mock",                //类型：String  必有字段  备注：渠道编号
    "productClassifyNo":"mock",                //类型：String  可有字段  备注：商品分类
    "discountType":"mock",                //类型：String  可有字段  备注：折扣类型 1.不打折 2.打折
    "auditStatus":"mock",                //类型：String  必有字段  备注：商品审核状态 必须为2 已审核
    "shelveStatus":"mock",                //类型：String  必有字段  备注：商品上架状态 必须为2 已上架
    "pageNum":"mock",                //类型：String  必有字段  备注：当前页码
    "pageSize":"mock"                //类型：String  必有字段  备注：页码大小
    }
 */
export const productShow = (data) => {
  return request({
		url: api+'/product/show',
    method: 'post',
    data
  })
};

/**
 * 商品展示 根据商品详情查询
 * {
    "activityProductNo":"mock",                //类型：String  必有字段  备注：活动商品编号
    "channelNo":"mock"                //类型：String  必有字段  备注：渠道编号
    }
 */
export const productDetail = (data) => {
  return request({
		url: api+'/product/detail',
    method: 'post',
    data
  })
};
/**
 * 商品展示 根据商品名称模糊查询
 */
export const productSearch= () => {
  return post({
    url: api+'/product/search'
  });
};
/**
 * 会员优惠券
 * {
    "memberNo":"mock",                //类型：String  必有字段  备注：会员编号
    "channelNo":"mock",                //类型：String  必有字段  备注：渠道编号
    "status":"mock"                //类型：String  必有字段  备注：优惠券状态 1 可用 2 已使用 3已过期
  }
 */ 
export const memberCoupons = data => {
	return request({
		url: api+'/member/coupons/show',
		method: 'post',
		data
	})
};
/**
 * 登录 商品优惠券
 *  {
    "memberNo":"mock",                //类型：String  必有字段  备注：会员编号
    "channelNo":"mock",                //类型：String  必有字段  备注：渠道编号
    "activityProductNo":"mock",                //类型：String  必有字段  备注：活动编号
    "amount":1                //类型：Number  必有字段  备注：商品金额
    } 
 */ 
export const memberCouponsAvailable= (data) => {
  return request({
		url: api+'/member/coupons/available',
		method: 'post',
		data
	})
};
//未登录 商品优惠券
export const couponsAvailable= (data) => {
  return request({
		url: api+'/coupons/available',
		method: 'post',
		data
	})
};

// 订单支付
export const orderPay= (data) => {
  return request({
		url: epi+'/v1/pay',
		method: 'post',
		data
	})
};
// 继续支付
export const orderRepay= (data) => {
  return request({
		url: epi+'/v1/repay',
		method: 'post',
		data
	})
};

// 订单列表

export const orderList= (data) => {
  return request({
		url: api+'/order/list',
		method: 'post',
		data
	})
};
// 订单详情/order/detail
export const orderDetail= (data) => {
  return request({
		url: api+'/order/detail',
		method: 'post',
		data
	})
};

export const themeInfo= (data) => {
  return request({
		url: api+'/theme/info',
		method: 'post',
		data
	})
};

// 购物车
/**修改购物车中的商品数量
 * {
    "memberNo":"BQ00000014",                //类型：String  必有字段  备注：会员编号
    "activityProductNo":"AT3003730129",                //类型：String  必有字段  备注：商品编号
    "productNum":100                //类型：Number  必有字段  备注：商品数量
}* **/
export const shoppChangeNum= (data) => {
  return request({
		url: cpi+'/shopping/cart/changeProductNum',
		method: 'post',
		data
	})
};

/**删除购物车
 * {
    "memberNo":"BQ00000017",                //类型：String  必有字段  备注：用户编号
    "deleteCartProNoList": [                //类型：Array  必有字段  备注：需要删除的商品编号集合
        "AT3005330085"                //类型：String  必有字段  备注：无
    ]
}* **/
export const shoppDeleteCart= (data) => {
  return request({
		url: cpi+'/shopping/cart/deleteCart',
		method: 'post',
		data
	})
};
/**查询购物车列表
 * {
    "memberNo":"BQ00000017",                //类型：String  必有字段  备注：用户编号
}* **/
export const getCartList= (data) => {
  return request({
		url: cpi+'/shopping/cart/getCartList',
		method: 'post',
		data
	})
};
/**添加购物车
 * {
    "memberNo":"BQ00000015",                //类型：String  必有字段  备注：用户编号
    "addCartDTOList": [                //类型：Array  必有字段  备注：添加到购物车数据的集合
        {                //类型：Object  必有字段  备注：无
            "isChecked":"0",                //类型：String  必有字段  备注：无
            "activityProductNo":"AT3004930055",                //类型：String  必有字段  备注：无
            "productNum":"2"                //类型：String  必有字段  备注：无
        },
        {                //类型：Object  必有字段  备注：无
            "isChecked":"0",                //类型：String  必有字段  备注：无
            "activityProductNo":"AT3006430097",                //类型：String  必有字段  备注：无
            "productNum":"3"                //类型：String  必有字段  备注：无
        }
    ]
}* **/
export const addToCart= (data) => {
  return request({
		url: cpi+'/shopping/cart/addToCart',
		method: 'post',
		data
	})
};
/**获取用户优惠卷信息
 *  {
    "memberNo":"BQ00000001",                //类型：String  必有字段  备注：用户编号
    "activityProductNoList": [                //类型：Array  必有字段  备注：商品编号列表
        "AT3005330086"                //类型：String  必有字段  备注：无
    ]
}* **/
export const getMemberCoupon= (data) => {
  return request({
		url: cpi+'/shopping/cart/getMemberCoupon',
		method: 'post',
		data
	})
};
/**获取购物车中的每个商品数量
 *  {
    "memberNo":"BQ00000001",                //类型：String  必有字段  备注：用户编号
}* **/
export const getShopProductNum= (data) => {
  return request({
		url: cpi+'/shopping/cart/getProductNum',
		method: 'post',
		data
	})
};
export const getShopCheckCart= (data) => {
  return request({
		url: cpi+'/shopping/cart/checkCart',
		method: 'post',
		data
	})
};


//免费领 
export const promotionFreeget= (data) => {
  return request({
		url: api+'/promotion/freeget',
		method: 'post',
		data
	})
};
//超值换购 
export const promotionExchangebuy= (data) => {
  return request({
		url: api+'/promotion/exchangebuy',
		method: 'post',
		data
	})
};

export const shoppingAmount= (data) => {
  return request({
		url: dpi+'/shopping/cart/discount/amount',
		method: 'post',
		data
	})
};

// 订单取消
export const orderCancel = (data) => {
  return request({
		url: api+'/order/cancel',
		method: 'post',
		data
	})
};

// 购物车猜你喜欢
export const productGuessLike = (data) => {
  return request({
		url: api+'/product/guess/like',
		method: 'post',
		data
	})
};

// h5授权
export const wechatAuth= (data) => {
	return request({
		url: bpi+'/wx/authorize/auth',
		method: 'post',
		data
	})
};
export const getShare= (data) => {
	return request({
		url: mpi+'/wx/signature/signatureUrl',   //√
		method: 'post',
		data
	})
};
export const getShopcoupon= (data) => {
	return request({
		url: dpi+'/shopping/cart/coupon',
		method: 'post',
		data
	})
};

export const blessList= (data) => {
	return request({
		url: api+'/blessing/theme/list',
		method: 'get',
		data
	})
};

export const geTaskList= (data) => {
	return request({
		url: api+'/member/daily/task/record/list',
		method: 'post',
		data
	})
}
// 获取会员等级
export const getMemberLevelList= (data)=>{
	return request({
		url: api+'/member/level/getMemberLevelInfoList',
		method: 'post',
		data
	})
}
// 获取日常任务增加完成目标数
export const geTaskAddList= (data)=>{
	return request({
		url: api+'/member/daily/task/record/add',
		method: 'post',
		data
	})
}

// 获取会员特权
export const getPrivilegeList= ()=>{
	return post({
		url: api+'/member/privilegeDescription/list',
	})
}

// 获取会员规则
export const geRulesList= ()=>{
	return post({
		url: api+'/member/rules/get',
	})
}

// 获取当前会员级别名称
export const getNowMemberLevel= (data)=>{
	return request({
		url: api+'/member/level/getOneMemberLevelInfo',
		method: 'post',
		data
	})
}


//收藏列表
export const memberCollectList= (data)=>{
	return post({
		url: api+'/member/collect/record/list',
		method: 'post',
		data
	})
}

// 收藏 新增
export const memberCollectAdd= (data)=>{
	return request({
		url: api+'/member/collect/record/add',
		method: 'post',
		data
	})
}
// 取消收藏
export const memberCollectCancel= (data)=>{
	return request({
		url: api+'/member/collect/record/cancel',
		method: 'post',
		data
	})
}

//提交反馈
export const feedbackSubmit= (data)=>{
	return request({
		url: api+'/member/feedback/add',
		method: 'post',
		data
	})
}




//会员信息修改 (添加生日)
export const memberInfoEdit= (data)=>{
	return request({
		url: api+'/member/customer/info/edit',
		method: 'post',
		data
	})
}

//会员信息修改 (添加生日)
export const memberCity = (data)=>{
	return request({
		url: api+'/city/selector/subordinate',
		method: 'post',
		data
	})
}


export const uploadImg = data => {
	return request({
		url:  api+'/oss/headupload',
		method: 'post',
		data
	})
};


//弹窗提示 生日优惠券
export const memberPopup = (data)=>{
	return request({
		url: api+'/user/popup',
		method: 'post',
		data
	})
}



// 首页icon 2020-05-07
export const iconList = (data)=>{
	return request({
		url: api+'/icon/list',
		method: 'post',
		data
	})
}
// 广告位-2020-5-6 
export const bannerLocation = (data)=>{
	return request({
		url: api+'/banner/location/list',
		method: 'post',
		data
	})
}
//  信息流配置2020-05-07
export const infoList = (data)=>{
	return request({
		url: api+'/info/flow/list',
		method: 'post',
		data
	})
}

// 收货地址列表 2020-09-23
export const addressList= (data)=>{
	return request({
		url: api+'/deliverty/address/list',
		method: 'post',
		data
	})
}

// 新增收货地址 2020-09-23
export const addressSave= (data)=>{
	return request({
		url: api+'/deliverty/address/save',
		method: 'post',
		data
	})
}

// 编辑收货地址 2020-09-23
export const addressUpdate= (data)=>{
	return request({
		url: api+'/deliverty/address/edit',
		method: 'post',
		data
	})
}
// 删除收货地址 2020-09-23
export const addressDelete= (data)=>{
	return request({
		url: api+'/deliverty/address/delete',
		method: 'post',
		data
	})
}
// 确认收货 2020-09-23
export const confirmReceipt= (data)=>{
	return request({
		url: api+'/order/confirmReceipt',
		method: 'post',
		data
	})
}

// 申请退款 2020-09-23
export const applyRefund= (data)=>{
	return request({
		url: epi+'/material/applyRefund',
		method: 'post',
		data
	})
}
// 退款原因
export const reasonSelectList= ()=>{
	return post({
		url: epi+'/material/refundReason',
	})
}
// 提交退款申请 2020-09-23
export const applyRefundSubmit= (data)=>{
	return request({
		url: epi+'/material/materialRefund',
		method: 'post',
		data
	})
}

// 退货列表 2020-10-14
export const refundList= (data) => {
	return request({
		url: epi+'/material/materialRefundList',
		method: 'post',
		data
	})
};

// 退货详情 2020-10-14
export const refundDetail= (data) => {
	return request({
		url: epi+'/material/materialRefundDetail',
		method: 'post',
		data
	})
};

// 撤销申请 2020-10-14
export const refundApplayCancel= (data) => {
	return request({
		url: epi+'/material/materialRefundCancel',
		method: 'post',
		data
	})
};

// 删除退款记录 2020-10-14
export const refundDeleteRecord= (data) => {
	return request({
		url: epi+'/material/refundRecordRemove',
		method: 'post',
		data
	})
};

// 修改退货申请 2020-10-14
export const refundRecordUpdate= (data) => {
	return request({
		url: epi+'/material/refundRecordUpdate',
		method: 'post',
		data
	})
};

// 统计待付款，待发货，待收货，退款/售后 数量
export const materialStatusList= (data) => {
	return request({
		url: epi+'/material/materialStatusList',
		method: 'post',
		data
	})
};
