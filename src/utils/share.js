import { getShare, geTaskAddList } from '@/api/index'
import { setAllToken ,getAllToken} from '@/utils/auth'

export function getShareInfo(){//如果分享的内容会根据情况变化，那么这里可以传入分享标题及url
    let tit = '一起来猜涨跌';
    // let fxUrl = 'https://prizejiangbei.92jiangbei.com/';
    let iconUrl = 'https://cdn.92jiangbei.com/newjiangbei/img/newImg/share.png';
    let linkUrl = location.href.split('#')[0];
    // let fxUrl = location.href.split('?')[0];
    // fxUrl = fxUrl.split('#')[0];
    let fxUrl ="";
    fxUrl = encodeURIComponent(location.href.split('#')[0])
    // let fxUrl = fxUrl;
    var data={//请求参数
      url : fxUrl,
      serviceName : 'WX.AUTH.JB'
    }
    // alert(JSON.stringify(data))
    getShare(data)//这里我写了一个公用的接口请求js，这里正常axios请求就可以，只要拿到数据都可以
        .then(res => {
          localStorage.setItem("jsapi_ticket", res.data);
          //拿到后端给的这些数据
          let appId = res.data.appId;
          let timestamp = res.data.timestamp;
          let nonceStr = res.data.nonceStr;
          let signature = res.data.signature;
          // alert(JSON.stringify(res.data))
          // eslint-disable-next-line no-undef
          wx.config({
            debug: false,// 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
            appId: appId,         // 必填，公众号的唯一标识，填自己的！
            timestamp: timestamp, // 必填，生成签名的时间戳，刚才接口拿到的数据
            nonceStr: nonceStr,   // 必填，生成签名的随机串
            signature: signature, // 必填，签名，见附录1
            jsApiList: [
              'onMenuShareTimeline',
              'onMenuShareAppMessage',
              'updateAppMessageShareData',
              'updateTimelineShareData'
            ]
          })
          
          // eslint-disable-next-line no-undef
          wx.ready(function () {
              //分享到朋友圈
              // eslint-disable-next-line no-undef
              wx.onMenuShareAppMessage({
                title: tit,   // 分享时的标题
                link: linkUrl,     // 分享时的链接
                imgUrl: iconUrl,    // 分享时的图标
                desc: '小呗带您逛集市，买礼券，送好友，玩转城市生活，点击立享更多折扣。',
                success: function () {
                  if(getAllToken("memberNo")){
                    let para = {
                       memberNo: getAllToken("memberNo"), 
                       taskNo: 'MDT00004'
                    };
                    geTaskAddList(para)
                      .then(res => {console.log("2")})
                      .catch(err => {});
                  }

                },
                cancel: function () {
                  console.log("取消分享");
                }
              });
              //分享给朋友
              // eslint-disable-next-line no-undef
              wx.onMenuShareTimeline({
                title: tit,
                desc: '小呗带您逛集市，买礼券，送好友，玩转城市生活，点击立享更多折扣。', 
                link: linkUrl,
                imgUrl:iconUrl,
                success: function () {
                  // alert(JSON.stringify(linkUrl))
                  if(getAllToken("memberNo")){
                    let para = {  
                        memberNo: getAllToken("memberNo"), 
                        taskNo: 'MDT00004'
                    };
                    // alert(JSON.stringify(memberNo))
                    geTaskAddList(para) //每分享一次 掉一次任务成功接口
                      .then(res => {
                      })
                      .catch(err => {});
                  }

                },
                cancel: function () {
                  console.log("取消分享");
                }
              });
          })
    })
}