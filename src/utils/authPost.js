import http from './http';

const post = (data) => {
  // const info = JSON.parse(localStorage.getItem('info'));
  data.method = 'POST';
  data.headers = {
    // 'x-access-token': info.token,
    // 'secret': info.secret
    'CHANNEL_NO':'1fa72678b069024789d5f555b0ad70f7'
  };
  return http(data);
};

export default post;
