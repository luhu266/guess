import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'

export function getToken() {
  return Cookies.get(TokenKey)
}

// export function setToken(token) {
//   return Cookies.set(TokenKey, token)
// }

// export function removeToken() {
//   return Cookies.remove(TokenKey)
// }

export function getAllToken(tokenName) {
  return Cookies.get(tokenName)
}

// export function setAllToken(tokenName,tokenKey) {
//   // return Cookies.set(tokenName, tokenKey,{ SameSite: 'None' })/
//   return Cookies.set(tokenName, tokenKey);
// }
//
// export function removeAllToken(tokenName) {
//   return Cookies.remove(tokenName)
// }

//解决cookie跨域问题
export function setAllToken(tokenName,tokenKey) {
  // return Cookies.set(tokenName, tokenKey,{ SameSite: 'None' })/
  return Cookies.set(tokenName, tokenKey,{domain: '.92jiangbei.com'});
}


export function removeAllToken(tokenName) {
  return Cookies.remove(tokenName,{domain: '.92jiangbei.com'})
}


//解决cookie跨域问题 设置domain 一级域名，一级域名及子域名皆可访问
export function setToken(key,value) {
  return Cookies.set(key, value, {domain: '.92jiangbei.com'})
}
//删除cookie时 指定域名删除
export function removeToken(key) {
  return Cookies.remove(key,{domain: '.92jiangbei.com'})
}
