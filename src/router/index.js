import Vue from 'vue';
import Router from 'vue-router';

const Login  = ()=> import ('../views/login/Login.vue');
const agree  = ()=> import ('../views/agree/agree.vue');
const Reward_rules  = ()=> import ('../views/rewardRules/Reward_rules.vue');
const Home  = ()=> import ('../views/Home.vue');
const Self = ()=> import ('../views/self/Self.vue');
const Act_Rules = ()=> import ('../views/Act_Rules.vue');
const Ranking = ()=> import('../views/Ranking.vue');
const Market_details = ()=> import ('../views/Market_details.vue')
const My_record = ()=> import ('../views/My_record.vue');
const Poster = ()=> import ('../views/Poster.vue');
const Prize_pool = ()=> import ('../views/Prize_pool.vue');
const Ercode = ()=> import('../views/Ercode.vue');
const History_Result = ()=> import('../views/History_Result.vue')
const Index = ()=> import ('../views/setSelf/index.vue')
const Name = ()=> import ('../views/setSelf/name.vue')
const UploadImg = ()=> import ('../views/setSelf/uploadImg.vue')


Vue.use(Router);


export const  routerMap =  [
		{ path: '/', redirect: '/Home' },
		{ path:'*', redirect:'/'},//路由按顺序从上到下，依次匹配。最后一个*能匹配全部，然后重定向到主页面
		
		{
			path: '/home',
			name: 'Home',
			component: Home
		},
		{
			path: '/login',
			name: 'Login',
			component: Login 
		},
		{
			path: '/agree',
			name: 'agree',
			component: agree  
		},
        {
			path: '/History_Result',
			name: 'History_Result',
			component: History_Result,
			
		},
        {
			path: '/self',
			name: 'Self',
			component: Self
        },
        {
			path: '/Act_Rules',
			name: 'Act_Rules',
			component: Act_Rules
		},
		{
			path: '/Ranking',
			name: 'Ranking',
			component: Ranking
		},
		{
			path: '/Market_details',
			name: 'Market_details',
			component: Market_details
		},
		{
			path: '/My_record',
			name: 'My_record',
			component: My_record
		},	
		{
			path: '/Poster',
			name: 'Poster',
			component: Poster
		},
		{
			path: '/Prize_pool',
			name: 'Prize_pool',
			component: Prize_pool
		},
		{
			path: '/Ercode',
			name: 'Ercode',
			component: Ercode
		},
		{
			path: '/Reward_rules',
			name: 'Reward_rules',
			component: Reward_rules
		},
		{
			path:'/setSelf/index',
			component:Index,
			children:[
				{path:'/setSelf/index',component:Index},
				{path:'/setSelf/name',component:Name},
				{path:'/setSelf/uploadImg',component:UploadImg},
			]
		},
	]

// 刷新页面方法
	const originalPush = Router.prototype.push;
	Router.prototype.push = function push(location) {
			return originalPush.call(this, location).catch(err => err)
	}

export default new Router({
	routes:routerMap
})