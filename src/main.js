/*
 * @Author: cy.hou
 * @Date: 2020-10-29 15:57:38
 * @LastEditTime: 2020-10-29 16:03:25
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \bc-guess-up-down-h5\src\main.js
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import common_css from './common/common.css'
import lib_flexible from 'lib-flexible'
import axios from 'axios'
import qs from 'qs'
import vuex from 'vuex'
import store from './store'
import ImgLink from './components/Global.vue'
import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload)



// or with options
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: 'dist/error.png',
  loading: 'dist/loading.gif',
  attempt: 1
})
Vue.prototype.ImgUrl = ImgLink.imgUrl;
Vue.prototype.ImgNewUrl = ImgLink.imgNewUrl;
Vue.prototype.$axios = axios;
Vue.prototype.qs = qs;

new Vue({
  router, 
  store,
  render: h => h(App)
}).$mount('#app')
