import { memberLogin } from '@/api'
import { getToken, setToken, removeToken ,setAllToken,getAllToken} from '@/utils/auth'

const user = {
  state: {
    memberNo:getAllToken("memberNo"),
    user: '',
    status: '',
    code: '',
    token: getToken(),
    list:[],
    name: '',
    avatar: '',
    introduction: '',
    roles: {},
    userroles: {},
    setting: {
      articlePlatform: []
    }
  },

  mutations: {
    SET_CODE: (state, memberNo) => {
      state.memberNo =  memberNo;
     
    },
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_LIST: (state, list) => {
      state.list = list
    },
    SET_INTRODUCTION: (state, introduction) => {
      state.introduction = introduction
    },
    SET_SETTING: (state, setting) => {
      state.setting = setting
    },
    SET_STATUS: (state, status) => {
      state.status = status
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_USER: (state, userroles) => {
      state.userroles = userroles
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    }
  },

  actions: {
    Loginout({ commit }, val) {
      commit('SET_CODE', '');
    },
    // 用户名登录
    LoginByUsername({ commit }, userInfo) {
     
      // const username = userInfo.username.trim()
      return new Promise((resolve, reject) => {
        memberLogin(userInfo).then(response => {
          console.log("userInfo=response,response.data",userInfo,response)  //source 4
          const data = response.data;
          if(data.code == "000"){
            // console.log(data)  //memberNo
            // commit('SET_TOKEN', data.data.token)
            // commit('SET_AVATAR', data.data.img);
           
            commit('SET_CODE', data.content.memberNo);
            setAllToken("accessToken",data.accessToken);
            setAllToken("avatar",data.content.avatar);
            setAllToken("user",data.content);
            setAllToken("memberNo",data.content.memberNo);
            setAllToken("mobile",data.content.mobile);
            let obj = {memberLevel:data.content.memberLevel,memberLevelNo:data.content.memberLevelNo,memberLevelName:data.content.memberLevelName};
            setAllToken("memberLevel",JSON.stringify(obj));
            
            let memberNo = data.content.memberNo
            sessionStorage.setItem("memberNo",JSON.stringify(memberNo))

            // if(data.content.showCoupon == 1){
            //   let coupons={couponsBatchNo:data.content.couponsBatchNo,couponsNo:data.content.couponsNo,couponsName:data.content.couponsName,
            //     couponsWorth:data.content.couponsWorth,couponsTime:data.content.couponsTime,couponsType:data.content.couponsType,}
            //   setAllToken("showCoupon",data.content.showCoupon);
            //   setAllToken("showCouponList",JSON.stringify(coupons));
            // }
            
          }
          resolve(data)
          
        }).catch(error => {
          reject(error)
        })
      })
    },
  
    // 获取用户信息
    GetUserInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        // eslint-disable-next-line no-undef
        getUserInfo(state.token).then(response => {
          if (!response.data) { // 由于mockjs 不支持自定义状态码只能这样hack
            reject('error')
          }
          const data = response.data

          if (data.roles && data.roles.length > 0) { // 验证返回的roles是否是一个非空数组
            commit('SET_ROLES', data.roles)
          } else {
            reject('getInfo: roles must be a non-null array !')
          }

          commit('SET_NAME', data.name)
          commit('SET_AVATAR', data.avatar)
          commit('SET_INTRODUCTION', data.introduction)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 登出
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        let para = {
          userId:Number(getAllToken("uid"))
        }
        // eslint-disable-next-line no-undef
        logout(para).then((res) => {
          console.log("登出res状态",res)
          resolve(res)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    },

    // 动态修改权限
    ChangeRoles({ commit }, role) {
      return new Promise(resolve => {
        commit('SET_TOKEN', role)
        setToken(role)
        // eslint-disable-next-line no-undef
        getUserInfo(role).then(response => {
          console.log("动态权限的role以及response数据",role,response)
          const data = response.data
          commit('SET_ROLES', data.roles)
          commit('SET_NAME', data.name)
          commit('SET_AVATAR', data.avatar)
          commit('SET_INTRODUCTION', data.introduction)
          resolve()
        })
      })
    }
  }
}

export default user
