import { getToken, setToken, removeToken ,setAllToken,getAllToken} from '@/utils/auth'
const app = {
  state: {
    sidebar: {
      tabbar: true
    },
    tabIndex:Number(getAllToken("tabIndex")) || 0,
  },
  mutations: {
    TOGGLE_SIDEBAR: state => {
      
    },
    CLOSE_SIDEBAR: (state, tabbar) => {
     
    },
    TOGGLE_TABBAR: (state,tabIndex) => {
      setAllToken("tabIndex",tabIndex.tabbarIndex);
      state.tabIndex =  tabIndex.tabbarIndex;
    },
  },
  actions: {
   
    updateTabbarIndex({ commit },tabIndex) {
      commit('TOGGLE_TABBAR',tabIndex)
    },
    toggleSideBar({ commit }) {
      commit('TOGGLE_SIDEBAR')
    },
    closeSideBar({ commit }, { tabbar }) {
      commit('CLOSE_SIDEBAR', tabbar)
    },

  }
}

export default app
