/*
 * @Author: cy.hou2020
 * @Date: 2020-10-19 10:27:14
 * @LastEditTime: 2020-10-19 11:29:07
 * @LastEditors: cy.hou2020
 * @Description: In User Settings Edit
 * @FilePath: \vuedemo_cy20201016f:\CY.02\No.1\No.2\Dev_time\Guess up and down\Guess up and down\config\test.env.js
 */
'use strict'
const merge = require('webpack-merge')
const devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  API_HOST: '"http://127.0.0.1:3000/"'   //test下的配置本地url
})
